<?php
	require "includes/links.php";
?>
<body>
<?php 
	require "header.php";
?>



<div class="container" style="position: absolute;width: 100%;height: 700px;z-index: -1;">  
	<h1>Sign up...</h1>
	<?php
		if(isset($_GET['error'])) {
			if($_GET['error'] == "emptyFields") {
				echo "Fill in all fields";
			} elseif ($_GET['error'] == "invalidMailUsername") {
				echo "invalid username and email";
			} elseif ($_GET['error'] == "invalidMail") {
				echo "Not a valid email address";
			} elseif ($_GET['error'] == "invalidName") {
				echo "Not a valid username";
			}  elseif ($_GET['error'] == "passwordCheck") {
				echo "Your passwords do not match!";
			}  elseif ($_GET['error'] == "userTaken") {
				echo "Username already exists!";
			}
		} elseif (isset($_GET['signup'])) {
			if($_GET['signup'] == "success") {
				header("Location: index.php");
			}
			
		}
	?>  
	<form action="handlers/signup.handler.php" method="POST">
		Email:<input type="email" name="email" placeholder="email..."><br />
		Username:<input type="text" name="username" placeholder="username..."><br />
		Password:<input type="Password" name="password" placeholder="password..."><br />
		Password:<input type="Password" name="password-repeat" placeholder="confirm password...">

		<button type="submit" name="signup-submit">signup</button>
	</form>
</div>
</body>
</html>