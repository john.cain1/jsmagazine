<footer>
	<div class="footer-top">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-6 col-cs-12 segment-one">
					<h3>Divinector</h3>
					<p>lorem ipsum dolor sit amet, consectetur yada yada yada.</p>
				</div>
				<div class="col-md-3 col-sm-6 col-cs-12 segment-two">
					<h2>Useful Links</h2>
					<ul>
						<li><a href="#">Upload</a></li>
						<li><a href="#">Explore</a></li>
						<li><a href="#">About</a></li>
						<li><a href="#">Newsletter</a></li>
						<li><a href="#">Account</a></li>
					</ul>
				</div>
				<div class="col-md-3 col-sm-6 col-cs-12 segment-three">
					<h2>Follow Us</h2>
					<p>Please follow us on our soical media profiles in order to keep updated.</p>
					<a href="#"><i class="fa fa-instagram"></i></a>
					<a href="#"><i class="fa fa-twitter"></i></a>
					<a href="#"><i class="fa fa-telegram"></i></a>
					<a href="#"><i class="fa fa-linkedin"></i></a>
				</div>
				<div class="col-md-3 col-sm-6 col-cs-12 segment-four">
					<h2>Our Newsletter</h2>
					<p>Lorem ipsum dolor sit amet.</p>
					<form method="POST" action="handlers/emaillist.php">
						<input type="email" name="email">
						<input type="submit" name="submit" value="subscribe">
					</form>
				</div>
			</div>
		</div>
	</div>
	<p class="footer-bottom-text">ALL RIGHTS reserved by &copy;JSMagazine 2020.</p>
</footer>

<style type="text/css">
	footer p{
		color: #999;
		line-height: 25px;
	}
	footer h2,h3{
		color: #fff;
	}
	footer h2{
		font-size: 18px;
	}
	footer .footer-top {
		background: #111;
		padding: 80px 0;
	}
	footer .segment-one h3{
		color: #fff;
		letter-spacing: 3px;
		margin: 10px;
	}
	footer .segment-two h2{
		color: #fff;
		text-transform: uppercase;
	}
	footer .segment-two h2:before{
		content: '|';
		color: #c65039;
		padding-right: 10px;
	}
	footer .segment-two ul{
		margin: 0;
		padding: 0;
		list-style: none;
	}
	footer .segment-two ul li {
		border-bottom: 1px solid rgba(255,255,255,0.3);
		line-height: 40px;
	}
	footer .segment-two ul li a{
		color: #999;
		text-decoration: none;
	}
	footer .segment-three h2{
		color: #fff;
		text-transform: uppercase;
	}
	footer .segment-three h2:before{
		content: '|';
		color: #c65039;
		padding-right: 10px;
	}
	footer .segment-three a{
		background: #494848;
		width: 40px;
		height: 40px;
		display: inline-block;
		border-radius: 50%;
	}
	footer .segment-three a i{
		font-size: 20px;
		color: #fff;
		padding: 10px 12px;
	}
	footer .segment-four h2{
		color #fff;
		text-transform: uppercase;
	}
	footer .segment-four h2:before{
		content: '|';
		color: #c65039;
		padding-right: 10px;
	}
	footer .segment-four form input[type=submit]{
		 background: #c65039;
		 border: none;
		 padding: 3px 15px;
		 margin-left: -5px;
		 color: #fff;
		 text-transform: uppercase;
	}
	.footer-bottom-text{
		text-align: center;
		background: #000;
		line-height: 75px;
	}

	footer {
		
	}


</style>