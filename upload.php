<?php
	require "includes/links.php";
?>
<script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
	tinymce.init({
		selector:'textarea',
		plugins: 'link, image, images_upload_handler'
	});
	</script>
<body>
<?php 
	require "header.php";
	if (isset($_SESSION['username'])) {
    	?>
    		<div class="container" style="position: absolute;width: 100%;height: 700px;z-index: -1;margin-top: 20px;">    
			  <div class="row" style="height: 700px;">
			    <div class="col-sm-2" style="height: 700px;">
			    </div>
			    <div class="col-sm-8" style="background-color: white;height: 700px;"> 
			      	<form action="handlers/upload.handler.php" method="POST">
			      		Title: <input type="text" name="title"><br />
			      		Brief Intro: <input type="text" name="intro"><br />
			      		Tags: <input type="text" name="tags"><br />
			      		Cover Photo: <input type="file" name="cover_image"><br />
			      		<textarea></textarea>
			      		
  						<input type="submit" name="submit-article" value="publish">
			      	</form>
			    </div>	
			  </div>
			</div>
    	<?
    } else {
    	header("Location: login.php");
    	exit();
    }
?>

</body>
</html>