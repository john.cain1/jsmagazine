<?php
	if(isset($_POST['signup-submit'])) {
		require 'connect.handler.php';

		$email = $_POST['email'];
		$username = $_POST['username'];
		$password = $_POST['password'];
		$passwordRepeat = $_POST['password-repeat'];

		if(empty($email) || empty($username) || empty($password) || empty($passwordRepeat)) {
			header("location: ../signup.php?error=emptyFields&username=".$username."&email=".$email);
			exit();
		} elseif(!filter_var($email, FILTER_VALIDATE_EMAIL) && !preg_match("/^[a-zA-Z0-9]*$/", $username)) {
			header("location: ../signup.php?error=invalidMailUsername");
			exit();
		} elseif(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			header("location: ../signup.php?error=invalidMail&username=".$username);
			exit();
		} elseif(!preg_match("/^[a-zA-Z0-9]*$/", $username)) {
			header("location: ../signup.php?error=invalidName&email=".$email);
			exit();
		} elseif($password !== $passwordRepeat) {
			header("location: ../signup.php?error=passwordCheck&email=".$email."&username=".$username);
			exit();
		} else {
			$sql = "SELECT uidUsers FROM users WHERE uidUsers=?";
			$stmt = mysqli_stmt_init($conn);
			if(!mysqli_stmt_prepare($stmt, $sql)){
				header("location: ../signup.php?error=sqlError");
				exit();
			} else {
				mysqli_stmt_bind_param($stmt, "s", $username);
				mysqli_stmt_execute($stmt);
				mysqli_stmt_store_result($stmt);
				$resultCheck = mysqli_stmt_num_rows($stmt);

				if($resultCheck > 0) {
					header("location: ../signup.php?error=userTaken&email=".$email);
					exit();
				} else {	
					$sql = "INSERT INTO users (uidUsers, emailUsers, pwdUsers) VALUES (?, ?, ?)";
					$stmt = mysqli_stmt_init($conn);
					if(!mysqli_stmt_prepare($stmt, $sql)){
						header("location: ../signup.php?error=sqlError");
						exit();
					} else {
						$hashedPwd = password_hash($password, PASSWORD_DEFAULT);

						mysqli_stmt_bind_param($stmt, "sss", $username, $email,  $hashedPwd);
						mysqli_stmt_execute($stmt);

						session_start();
						$_SESSION['username'] = $_POST[''];
						header("location: ../signup.php?signup=success");
						exit();
					}
				}
			}
		}
		mysqli_stmt_close($stmt);
		mysqli_close($conn);
	} else {
		header("location: ../signup.php");
		exit(); 
	}