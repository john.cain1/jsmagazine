<?php
	if(isset($_POST['login-submit'])) {
		require 'connect.handler.php';
		// <input type="input" name="email" placeholder="username/email">
		// <input type="password" name="password" placeholder="password">
		// <button type="submit" name="login-submit">Login</button>
		$email = $_POST['email'];
		$password = $_POST['password'];

		if(empty($email) || empty($password)) {
			header("Location: ../login.php?error=emptyFields");
			exit();

		} else {
			$sql = "SELECT * FROM users WHERE uidUsers = ? OR emailUsers = ?;";
			$stmt = mysqli_stmt_init($conn);
			if(!mysqli_stmt_prepare($stmt, $sql)) {
				header("Location: ../login.php?error=sqlError");
				exit();
			} else {
				mysqli_stmt_bind_param($stmt, "ss", $email, $email);
				mysqli_stmt_execute($stmt);
				$result = mysqli_stmt_get_result($stmt);
				if($row = mysqli_fetch_assoc($result)) {
					$pwdCheck = password_verify($password, $row['pwdUsers']);
					if($pwdCheck == false) {
						header("Location: ../login.php?error=wrongPassword");
						exit();
					} elseif($pwdCheck == true) {
						session_start();
						$_SESSION['userId'] = $row['idUsers'];
						$_SESSION['username'] = $row['uidUsers'];

						header("Location: ../index.php?login=success");
						exit();
					} else {
						header("Location: ../login.php?error=wrongPassword");
						exit();
					}
				} else {
					header("Location: ../login.php?error=noUser");
					exit();
				}
			}
		}
	} else {
		header("Location: ../login.php");
		exit();
	}
?>