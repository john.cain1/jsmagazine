<?php
	require "handlers/connect.handler.php";
    session_start();
?>

<div>
    <nav class="navbar navbar-default navigation-clean-button">
        <div class="container">
            <div class="navbar-header"><a class="navbar-brand" href="index.php"><img src="images/js_logo.jpg"><p class="magazine_title">Magazine</p></a>
                <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
            </div>
            <div class="collapse navbar-collapse" id="navcol-1">
                <ul class="nav navbar-nav">
                    <li role="presentation"><a href="upload.php">Upload</a></li>
                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="explore.php">Explore <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li role="presentation"><a href="explore.php">Trending Articles</a></li>
                            <li role="presentation"><a href="explore.php">New Articles</a></li>
                        </ul>
                    </li>
                    <li role="presentation"><a href="about.php">About</a></li>
                    <li role="presentation"><a href="newsletter.php">Newsletter</a></li>

                </ul>
                <?php
                    if (isset($_SESSION['username'])) {
                        ?>
                        <!-- <p class="navbar-text navbar-right actions"><form class="navbar-text navbar-right actions" action="handlers/logout.handler.php" method="post"><button type="submit" name="logout-submit">logout</button></form></p> -->
                        <p class="navbar-text navbar-right actions"><a class="btn btn-default action-button" role="button" href="signup.php" style="">Hello <?php echo $_SESSION['username']; ?></a></p>
                        <?php
                    } else {
                        ?>
                        <p class="navbar-text navbar-right actions"><a class="navbar-link login" href="login.php">Log In</a> <a class="btn btn-default action-button" role="button" href="signup.php">Sign Up</a></p>
                        <?php
                    }
                ?>
                
            </div>
        </div>
    </nav>
</div>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
