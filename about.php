<?php
	require "includes/links.php";
?>
<body>
<?php 
	require "header.php";
?>
<style type="text/css">
	.magazine_title{
		color: white;
	}
</style>
<div class="container" style="width: 100%;height: 700px;z-index: -1;">    
  <div class="row v-align" style="height: 700px;background-image: url('images/about_placeholder.jpg');background-size: auto 100%;">
    <div class="col-sm-7" style="height: 700px;">
    </div>
    <div class="col-sm-5" style="background-color: #F8DC3D;height: 700px;"> 
      	<h1 style="font-size: 3.4em;">//Hello, world.</h1>
      	<div style="text-align: center;margin-top: 50px;margin-left: 50px;margin-right: 20px;">
      		<i class="fa fa-quote-right fa-3x" aria-hidden="true" style="margin-right: 20px;"></i><br />
      		<p style="font-size: 2em;text-align: left;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p> <br />
      		<p style="text-align: right;font-size: 1.2em;margin-right: 20px;"><i>- John Cain, founder</i></p>
      	</div>
      	
    </div>
  </div>
  <div class="row v-align">
    <?php
      require "footer.php";
    ?>    
  </div>
</div>

</body>

</html>

<style type="text/css">
</style>